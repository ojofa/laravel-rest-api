### Laravel REST API using TDD (Docker, GitLab CI/CD, AWS)

### Table of Contents

1. [Scenario](#scenario)
2. [Requirements](#requirements)
   1. [Functional requirements](#functional-requirements)
   2. [Non-Functional requirements](#non-functional-requirements)
   3. [System Constraints](#system-constraints)
3. [Getting Started](#getting-started)
   2. [Setup](#setup)
      1. [Local Setup](#local-setup)
      2. [Deployment Setup](#deployment-setup)
         1. [GitLab CI/CD](#Gitlab-continuous-integration-continuous-deployment)
         2. [AWS](#aws)
4. [Application Design and Development](#application-design-and-development)
   1. [Review - Best Practices](#review-best-practices)
5. [Conclusion]($conclusion)
   1. [Next Steps](#next-steps)
   2. [Resources](#resources)
   

##

1. #### Scenario
   - `TODO`

2. #### Requirements
    - `TODO`

3. #### Getting Started
    - `TODO`

4. #### Application Design and Development
    - `TODO`

5. #### Conclusion
- `TODO`